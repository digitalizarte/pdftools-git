//
// MyClass.cs
//
// Author:
//       Damian Eiff <damian@digitalizarte.com>
//
// Copyright (c) 2013 
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
namespace PDFLib
{
    using System;
    using System.IO;
    using iTextSharp.text.pdf;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Globalization;
    using iTextSharp.text.pdf.parser;

    /// <summary>
    /// PDF image tool.
    /// </summary>
    public class PDFImageTool
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PDFLib.PDFImageTool"/> class.
        /// </summary>
        public PDFImageTool()
        {
            // Info: http://stackoverflow.com/questions/802269/extract-images-using-itextsharp
            // Info: http://stackoverflow.com/questions/10689382/extract-image-from-a-particular-page-in-pdf-using-itextsharp-4-0

        }

        public void ExtractImages(string source, string outputPath)
        {

            if (source == null)
                throw new ArgumentNullException("source");

            if (source == String.Empty)
                throw new ArgumentException("source");

            if (String.IsNullOrWhiteSpace(source))
                throw new ArgumentException("source");

            if (!File.Exists(source))
                throw new FileNotFoundException("File not found", source);

            // NOTE:  This will only get the first image it finds per page.
            RandomAccessFileOrArray raf = new RandomAccessFileOrArray(source);
            using (PdfReader pdf = new PdfReader(source))
            {
                try
                {
                    for (int pageNumber = 1, l = pdf.NumberOfPages; pageNumber <= l; pageNumber++)
                    {
                        PdfDictionary pg = pdf.GetPageN(pageNumber);
                        // recursively search pages, forms and groups for images.
                        PdfObject obj = FindImage(pg);
                        if (obj != null)
                        {
                            PRIndirectReference indirectReference = (PRIndirectReference)obj;
                            int xRefIndex = Convert.ToInt32(indirectReference.Number.ToString(CultureInfo.InvariantCulture));
                            PdfObject pdfObj = pdf.GetPdfObject(xRefIndex);
                            PdfStream pdfStrem = (PdfStream)pdfObj;

                            byte[] bytes = PdfReader.GetStreamBytesRaw((PRStream)pdfStrem);
                            if ((bytes != null))
                            {
                                using (MemoryStream memStream = new MemoryStream(bytes))
                                {
                                    memStream.Position = 0;
                                    Image img2 = Image.FromStream(memStream);
                                    if (!Directory.Exists(outputPath))
                                        Directory.CreateDirectory(outputPath);
                                    string path = Path.Combine(outputPath, String.Format(@"{0}.png", pageNumber));
                                    img2.Save(path, ImageFormat.Png);
                                }
                            }
                        }
                    }
                } catch (Exception ex)
                {
                    ex.Data["Source"] = source;
                    ex.Data["outputPath"] = outputPath;
                    throw;
                } finally
                {
                    // pdf.Close();
                    raf.Close();
                }
            }
        }

        private  PdfObject FindImage(PdfDictionary pdfDictionary)
        {
            if (pdfDictionary == null)
                throw new ArgumentNullException("pdfDictionary");

            PdfObject pdfResource = pdfDictionary.Get(PdfName.RESOURCES);
            PdfDictionary res = (PdfDictionary)PdfReader.GetPdfObject(pdfResource);

            PdfObject pdfXObject = res.Get(PdfName.XOBJECT);
            PdfDictionary xobj = (PdfDictionary)PdfReader.GetPdfObject(pdfXObject);
            if (xobj != null)
            {
                foreach (PdfName name in xobj.Keys)
                {
                    PdfObject obj = xobj.Get(name);
                    if (obj.IsIndirect())
                    {
                        PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);
                        PdfObject pdfObject = tg.Get(PdfName.SUBTYPE);
                        PdfName type = (PdfName)PdfReader.GetPdfObject(pdfObject);

                        if (PdfName.IMAGE.Equals(type) /* image at the root of the pdf */)
                        {
                            return obj;
                        } else if (PdfName.FORM.Equals(type) /* image inside a form */)
                        {
                            return FindImage(tg);
                        } else if (PdfName.GROUP.Equals(type) /* image inside a group */)
                        {
                            return FindImage(tg);
                        }
                    }
                }
            }
            return null;
        }
    }
}
